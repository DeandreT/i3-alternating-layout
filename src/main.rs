use i3ipc::{
    event::{inner::WindowChange, Event},
    reply::{
        Node,
        NodeLayout::{Stacked, Tabbed},
    },
    I3Connection, I3EventListener, Subscription,
};

fn find_parent<'a>(node: &'a Node, parent: Option<&'a Node>, window_id: i64) -> Option<&'a Node> {
    if node.id == window_id {
        if let Some(p) = parent {
            return Some(p);
        }
    }
    for child in &node.nodes {
        if let Some(p) = find_parent(&child, Some(&node), window_id) {
            return Some(p);
        }
    }
    None
}

fn splitter(conn: &mut I3Connection, window: Node) {
    if let Some(p) = find_parent(&conn.get_tree().unwrap(), None, window.id) {
        if p.layout != Stacked && p.layout != Tabbed {
            let width = window.window_rect.2;
            let height = window.window_rect.3;
            if height > width {
                conn.run_command("split v").unwrap();
            } else {
                conn.run_command("split h").unwrap();
            }
        }
    }
}

fn main() {
    loop {
        if let Ok(mut listener) = I3EventListener::connect() {
            listener
                .subscribe(&[Subscription::Window])
                .expect("Couldn't subscribe to window events");
            if let Ok(mut conn) = I3Connection::connect() {
                while let Ok(e) = listener
                    .listen()
                    .next()
                    .expect("Listener had no event. Reconnecting")
                {
                    if let Event::WindowEvent(e) = e {
                        if e.change == WindowChange::Focus {
                            splitter(&mut conn, e.container)
                        }
                    }
                }
            }
        }
    }
}
